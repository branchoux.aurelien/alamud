# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2,Event3

class LightOnEvent(Event2):
    NAME = "light-on"

    def perform(self):
        if not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("light-on.failed")
        self.inform("light-on")

class LightOnWithEvent(Event3):											#Modifié
    NAME = "light-with"							
    def perform(self):
        if not self.object.has_prop("flammable"):						#Modifié
            self.fail()
            return self.inform("light-with.failed")
        self.inform("light-with")										#Modifié
        
class LightOffEvent(Event2):
    NAME = "light-off"

    def perform(self):
        if not (self.object.has_prop("lightable") or self.object.has_prop("flammable")):
            self.fail()
            return self.inform("light-off.failed")
        self.inform("light-off")
