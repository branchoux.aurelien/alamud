# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import LightOnEvent, LightOffEvent, LightOnWithEvent

class LightOnAction(Action2):
    EVENT = LightOnEvent
    ACTION = "light-on"
    RESOLVE_OBJECT = "resolve_for_use"

class LightOnWithAction(Action3):										#Modifié
    EVENT = LightOnWithEvent											#Modifié
    ACTION = "light-with"
    RESOLVE_OBJECT = "resolve_for_use"										#Modifié
    RESOLVE_OBJECT2 = "resolve_for_use"									#Modifié

class LightOffAction(Action2):
    EVENT = LightOffEvent
    ACTION = "light-off"
    RESOLVE_OBJECT = "resolve_for_use"
